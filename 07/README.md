# Docker Swarm

This is the presentation presentation about *Docker Swarm* 

## Scripts

The `bin/` folder has scripts to startup basic installation using [docker-machine](https://docs.docker.com/machine/overview/)

## Demo Videos

The `video/` folder has the dump for [asciinema](https://asciinema.org)'s videos, play it with:

```
asciinema play videos/05-update.json
```

command.

## References

* [Docker Swarm For High Availability | Docker Tutorial | DevOps Tutorial | Edureka](https://www.youtube.com/watch?v=Ceqb53EXANk)
* [15 minutes with Docker Swarm Mode](https://medium.com/@LachlanEvenson/15-minutes-with-docker-swarm-mode-e6c38b9dafa0)


