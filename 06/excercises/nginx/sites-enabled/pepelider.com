# Default server configuration
#
server {
	#listen 80 default_server;
	#listen [::]:80 default_server;

	root /var/www/pepelider.com;

	# Add index.php to the list if you are using PHP
	index index.php index.html;

	server_name pepelider.com www.pepelider.com;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ /index.php?$args;
	}

        location ~*  \.(jpg|jpeg|png|gif|ico)$ {
        	log_not_found off;
        	access_log off;
        }

	# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
	#
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
	}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	location ~ /\.ht {
		deny all;
	}

	# Deny public access to wp-config.php
	location ~* wp-config.php {
    		deny all;
	}

	# Deny access to wp-login.php
	#location = /sitio/wp-login.php {
    	#	limit_req zone=login burst=1 nodelay;
    	#	fastcgi_pass unix:/var/run/php5-fpm.sock;
	#}



    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/pepelider.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/pepelider.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}


server {
    if ($host = pepelider.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot

    if ($host = www.pepelider.com) {
        return 301 https://pepelider.com/$request_uri;
    } # managed by Certbot

    server_name pepelider.com www.pepelider.com;
    listen 80;
    return 404; # managed by Certbot


}
