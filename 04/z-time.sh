#!/bin/bash
# This script ask for a time and notice the user when time end.
TIME=$(zenity --entry --title="Alert time" --text="Enter a duration for the timer.\n\n Use 5s for 5 seconds, 10m for 10 minutes, or 2h for 2 hours.")
sleep $TIME
zenity --info --title="Timer Complete" --text="The timer is over.\n\n It has been $TIME."
